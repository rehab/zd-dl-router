var global_download_path;
var ticketId;
var ticketTitle;
var ticket_title_tolower;

/**
 * Transform a string to a safe folder name by stripping any illegal characters
 * not allowed by the filesystem
 * @param {*} string The string to process
 * @param {*} toLower If the string should be converted to lowercase
 */
function transformToSafeFolderName(string, toLower = false) {

    // Return an empty string if we got passed nothing
    if(!string) {
        return "";
    }

    // Remove any HTML entities in the string
    string = string.replace (/&#{0,1}[a-z0-9]+;/ig, "");

    // Regex any illegal characters out of the string
    string = string.replace(/[|&;$%@"<>()+,\/\\~\:\?]/g, "");

    // Remove any multiple spaces
    string = string.replace(/\s\s+/g, " ");

    // Convert to lowercase if desired
    if(toLower) {
        string = string.toLowerCase();;
    }

    return string;
}

/**
 * Process a message from the content script and load the local variables
 * in the background script
 * @param {*} request
 */
function processContentMessage(request) {

    if(request.ticketTitle) {
        ticketTitle = request.ticketTitle;
    }
    if(request.ticketId) {
        ticketId = request.ticketId;
    }
}

/**
 * When the extension is initialised, set any default settings up for the first time
 */
chrome.storage.sync.get({download_path: 'zd-%TICKET_ID%/', ticket_title_tolower: false}, function(settings) {
    global_download_path = settings.download_path;
    ticket_title_tolower = settings.ticket_title_tolower;

    // Set defaults in storage, in case we are loading the extension for the first time
    chrome.storage.sync.set({download_path: global_download_path, ticket_title_tolower: ticket_title_tolower});
});

/**
 * Listen for any changes made to the extension options. If any changes are
 * made, update the internal variables.
 */
chrome.storage.onChanged.addListener(function(changes, area) {
    if (area == 'sync') {
        if('download_path' in changes) {
            global_download_path = changes.download_path.newValue;
        }
        if('ticket_title_tolower' in changes) {
            ticket_title_tolower = changes.ticket_title_tolower.newValue;
        }
    }
});

chrome.downloads.onDeterminingFilename.addListener(function(item, suggest) {

    if(item.url.includes('zendesk.com')) {

        var download_path = global_download_path;

        
        // HOTFIX: Grab the ticket ID from the referrer if it is available
        ticketId = item.referrer.substring(item.referrer.lastIndexOf('/') + 1);
        
        // Replace variables in download path (and strip illegal characters)
        if(download_path.includes('%TICKET_ID%')) {
            download_path = download_path.replace('%TICKET_ID%', ticketId);
        }

        if(download_path.includes('%TICKET_TITLE%')) {
            download_path = download_path.replace('%TICKET_TITLE%', transformToSafeFolderName(ticketTitle, ticket_title_tolower));
        }

        full_download_path = download_path + item.filename;

        // Set the folder
        suggest({filename: full_download_path});
    }

});

// Find ticket number and open Parent Folder
function showParentFolder() {
    chrome.downloads.search({filenameRegex: ticketId}, function(e) {

        // Only attempt to open the folder if we have results
        if(Array.isArray(e) && e.length) {
            chrome.downloads.show(parseInt(e[0]["id"]));
        }
    });
};

// Show Parent Folder when extension icon is clicked
chrome.browserAction.onClicked.addListener(function(tab) {showParentFolder()});

// Listener for receiving the current ticket title
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

    // Don't process if the sender tab is not active
    if(!sender.tab.active) return;

    chrome.windows.getCurrent({}, function(window) {

        // Don't process if the sender window is not active
        if(window.id !== sender.tab.windowId) return;

        processContentMessage(request);
    });
});

/**
 * Watch for any tab changes in the browser. If any are detected, force a
 * refresh of the ticket fields
 */
chrome.tabs.onSelectionChanged.addListener(function(tabId) {
    chrome.tabs.get(tabId, function(tab) {
        if(tab && tab.url.includes('zendesk.com')) {
            chrome.tabs.sendMessage(tab.id, {action: "refresh"});
        }
    });
});

chrome.windows.onFocusChanged.addListener(function(windowId) {

    // If window ID is zero, then we are looking at a developer window
    if(windowId <= 0) {
        return;
    }

    chrome.tabs.getSelected(windowId, function(tab) {
        if(tab && tab.url.includes('zendesk.com')) {
            chrome.tabs.sendMessage(tab.id, {action: "refresh"});
        }
    });
});