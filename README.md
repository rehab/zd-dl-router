# zd-dl-router

A Chrome extension that routes downloads from Zendesk into separate folders

Does your download folder look like this?

<img src="images/before.png" alt="before" style="zoom:50%;float:left;" />

Make it look like this:

<img src="images/after.png" alt="after" style="zoom: 50%; float: left;" />


## Installing

The extension can be installed via the Chrome Web Store. 

https://chrome.google.com/webstore/detail/zendesk-download-router/pgfhacdbkdeppdjgighdeejjfneifkml

## Building

1. Bump the `version` in `manifest.json`.
2. Run `git archive --worktree-attributes -o 'zd-dl-router.zip' HEAD` to package the extension
3. Upload the new extension to the [Chrome Webstore Developer Dashboard](https://chrome.google.com/webstore/developer/dashboard)

## Development

This extension is not automatically packed yet. Clone this project and then follow the instructions here https://stackoverflow.com/questions/24577024/install-chrome-extension-not-in-the-store


