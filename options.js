

// Saves options to chrome.storage
function save_options() {
    var status = document.getElementById('status');
    var download_path = document.getElementById('download_path').value;
    var ticket_title_tolower = document.getElementById('ticket_title_tolower').checked;

    // Add trailing slash if it doesn't exist
    download_path += download_path.endsWith("/") ? "" : "/"

    chrome.storage.sync.set({download_path: download_path, ticket_title_tolower: ticket_title_tolower}, function() {
        
        // Update status to let user know options were saved.
        flashAlert('✔️ Options saved', 'alert-success');

        // Reload the options
        restore_options();
    });
}

function flashAlert(msg, type) {
    var status = document.getElementById('status');

    status.classList = "";
    status.classList.add("alert");
    status.classList.add(type);
    status.innerHTML = msg;
    status.style.display = "block"

    // Auto hide the message if a success
    if(type == 'alert-success') {
        setTimeout(function() {
            status.style.display = "none"
        }, 1000);
    }
}
  
// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
    chrome.storage.sync.get({
        download_path: 'download_path',
        ticket_title_tolower: 'ticket_title_tolower'
    }, function(items) {
        document.getElementById('download_path').value = items.download_path;
        document.getElementById('ticket_title_tolower').checked = items.ticket_title_tolower;
    });
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);
