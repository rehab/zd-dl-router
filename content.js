/**
 * Once the Zendesk ticket is loaded, extract the necessary fields and send
 * them back to the background script
 */
function extractTicketFields() {
    header = document.getElementById('branding_header')

    title = header.querySelector('div[data-selected="true"] div[data-test-id="header-tab-title"]');
    if(title != null) {
        chrome.runtime.sendMessage({ticketTitle: title.innerHTML});
    }

    id = header.querySelector('div[data-selected="true"]');
    if(id != null && id.hasAttribute('data-entity-id')) {
        chrome.runtime.sendMessage({ticketId: id.getAttribute('data-entity-id')});
    }
}

/**
 * Observe any changes made to the page and trigger a field extraction
 * if changes are detected
 */
var observer = new MutationObserver(function (mutations) {
    mutations.forEach(function (mutation) {
        if(mutation.type === 'attributes' && mutation.attributeName === 'data-selected' && mutation.target.getAttribute('data-selected') === 'true') {
            extractTicketFields();
            return; // this mutation has been handled already
        }

        if (!mutation.addedNodes || mutation.addedNodes.length === 0) {
            return;
        }
        for (var i = 0; i < mutation.addedNodes.length; i++) {
            const innerText = mutation.addedNodes[i].innerText;

            if (!innerText) continue;

            // If we detect a change to this ID, we can assume that Ember has loaded
            // a ticket tab
            if (innerText.match(/\n#\d+$/)) {
                extractTicketFields();
                return; // this mutation has been handled already
            }
        }
    });
});

observer.observe(document.body, {
    attributes: true,
    childList: true,
    subtree: true
});

/**
 * Listen for any messages from the background script
 */
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.action == "refresh") {
        extractTicketFields();
    }
});